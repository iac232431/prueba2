package ntorno;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class App {

	protected Shell shlSwtApplication;
	private Text text;
	private Text text_1;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			App window = new App();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlSwtApplication.open();
		shlSwtApplication.layout();
		while (!shlSwtApplication.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlSwtApplication = new Shell();
		shlSwtApplication.setSize(605, 434);
		shlSwtApplication.setText("SWT Application");
		
		Label lblNombrex = new Label(shlSwtApplication, SWT.NONE);
		lblNombrex.setBounds(20, 37, 61, 15);
		lblNombrex.setText("NombreX2");
		
		Button btnEnviar = new Button(shlSwtApplication, SWT.NONE);
		btnEnviar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnEnviar.setBounds(20, 115, 75, 25);
		btnEnviar.setText("Enviar");
		
		text = new Text(shlSwtApplication, SWT.BORDER);
		text.setBounds(87, 31, 76, 21);
		
		Label lblEdad = new Label(shlSwtApplication, SWT.NONE);
		lblEdad.setBounds(20, 64, 55, 15);
		lblEdad.setText("Edad");
		
		Spinner spinner = new Spinner(shlSwtApplication, SWT.BORDER);
		spinner.setBounds(87, 57, 47, 22);
		
		text_1 = new Text(shlSwtApplication, SWT.BORDER);
		text_1.setBounds(87, 88, 76, 21);
		
		Label lblNumero = new Label(shlSwtApplication, SWT.NONE);
		lblNumero.setBounds(20, 94, 55, 15);
		lblNumero.setText("Numero");
		
		Button btnDoblar = new Button(shlSwtApplication, SWT.NONE);
		btnDoblar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Doblando");
				Integer x = Integer.parseInt(text_1.getText());
				x=x*2;
				text_1.setText(x.toString());
			}
		});
		btnDoblar.setBounds(179, 84, 75, 25);
		btnDoblar.setText("Doblar");

	}
}
