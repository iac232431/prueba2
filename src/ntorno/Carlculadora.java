package ntorno;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class Carlculadora {

	protected Shell shell;
	private Text text;
	double x=0;
	int op=0;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Carlculadora window = new Carlculadora();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(362, 320);
		shell.setText("SWT Application");
		
		text = new Text(shell, SWT.BORDER);
		text.setBounds(27, 10, 309, 53);
		
		Button button = new Button(shell, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("9");
			}
		});
		button.setBounds(141, 87, 39, 25);
		button.setText("9");
		
		Button button_1 = new Button(shell, SWT.NONE);
		button_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("7");
			}
		});
		button_1.setBounds(27, 87, 39, 25);
		button_1.setText("7");
		
		Button button_2 = new Button(shell, SWT.NONE);
		button_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("8");
			}
		});
		button_2.setBounds(84, 87, 39, 25);
		button_2.setText("8");
		
		Button button_1_1 = new Button(shell, SWT.NONE);
		button_1_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("4");
			}
		});
		button_1_1.setText("4");
		button_1_1.setBounds(27, 129, 39, 25);
		
		Button button_1_2 = new Button(shell, SWT.NONE);
		button_1_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("5");
			}
		});
		button_1_2.setText("5");
		button_1_2.setBounds(84, 129, 39, 25);
		
		Button button_1_3 = new Button(shell, SWT.NONE);
		button_1_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("6");
			}
		});
		button_1_3.setText("6");
		button_1_3.setBounds(141, 129, 39, 25);
		
		Button button_1_4 = new Button(shell, SWT.NONE);
		button_1_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("1");
			}
		});
		button_1_4.setText("1");
		button_1_4.setBounds(27, 173, 39, 25);
		
		Button button_1_5 = new Button(shell, SWT.NONE);
		button_1_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("2");
			}
		});
		button_1_5.setText("2");
		button_1_5.setBounds(84, 173, 39, 25);
		
		Button button_1_6 = new Button(shell, SWT.NONE);
		button_1_6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("3");
			}
		});
		button_1_6.setText("3");
		button_1_6.setBounds(141, 173, 39, 25);
		
		Button button_3 = new Button(shell, SWT.NONE);
		button_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.insert("0");
			}
		});
		button_3.setBounds(27, 214, 153, 25);
		button_3.setText("0");
		
		Button button_4 = new Button(shell, SWT.NONE);
		button_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				x=Double.parseDouble(text.getText());
				op=1;
				text.setText("");
			}
		});
		button_4.setBounds(210, 87, 56, 25);
		button_4.setText("+");
		
		Button button_4_1 = new Button(shell, SWT.NONE);
		button_4_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				x=Double.parseDouble(text.getText());
				op=2;
				text.setText("");
			}
		});
		button_4_1.setText("-");
		button_4_1.setBounds(210, 129, 56, 25);
		
		Button button_4_2 = new Button(shell, SWT.NONE);
		button_4_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				x=Double.parseDouble(text.getText());
				op=3;
				text.setText("");
			}
		});
		button_4_2.setText("*");
		button_4_2.setBounds(280, 87, 56, 25);
		
		Button button_4_3 = new Button(shell, SWT.NONE);
		button_4_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				x=Double.parseDouble(text.getText());
				op=4;
				text.setText("");
			}
		});
		button_4_3.setText("/");
		button_4_3.setBounds(280, 129, 56, 25);
		
		Button button_5 = new Button(shell, SWT.NONE);
		button_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				switch(op) {
				case 1:
					text.setText(x+Integer.parseInt(text.getText())+"");
				break;
				
				case 2:
					text.setText(x-Integer.parseInt(text.getText())+"");
				break;
				
				case 3:
					text.setText(x*Integer.parseInt(text.getText())+"");
				break;
				
				case 4:
					text.setText(x/Integer.parseInt(text.getText())+"");
				break;
				}
				x=0;
			}
		});
		button_5.setBounds(210, 173, 126, 25);
		button_5.setText("=");
		
		Button btnC = new Button(shell, SWT.NONE);
		btnC.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText("");
				x=0;
			}
		});
		btnC.setBounds(210, 214, 75, 25);
		btnC.setText("C");

	}
}
